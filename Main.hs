import           Hedgehog            (property, (===))
import           Test.Tasty          (defaultMain, testGroup)
import           Test.Tasty.Hedgehog (testProperty)

i :: a -> a
i x = x

main :: IO ()
main = defaultMain $ testGroup ""
    [ testProperty "I x = x" $ property $ do
        let x = ()
        i x === x
    ]
